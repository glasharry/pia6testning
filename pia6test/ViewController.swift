//
//  ViewController.swift
//  pia6test
//
//  Created by Bill Martensson on 2017-03-14.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var numberTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Ändring
        /*
        var someone = person()
        someone.getName()
        
        someone.addName(fname: "xxx")
 */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func doubleSomething(inText : String) -> String
    {
        var number = Int(inText)
        
        number = number! * 2
        
        let doubleNumberText = String(describing: number!)
        
        return doubleNumberText
    }
    
    
    @IBAction func letsDouble(_ sender: Any) {
        
        numberTextfield.text = doubleSomething(inText: numberTextfield.text!)
        
    }
        
}

