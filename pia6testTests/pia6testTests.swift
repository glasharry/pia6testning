//
//  pia6testTests.swift
//  pia6testTests
//
//  Created by Bill Martensson on 2017-03-16.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import XCTest

class pia6testTests: XCTestCase {
    
    var vc : ViewController?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        vc = ViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let someone = person()
        
        
        XCTAssert(someone.tripleNumber(inNumber: 0) == 0)
        XCTAssert(someone.tripleNumber(inNumber: 3) == 9)
        XCTAssert(someone.tripleNumber(inNumber: 5) == 10)
    }
    
    func testPersonName()
    {
        let someone = person()
        
        XCTAssert(someone.getName() == "")
        
        let addNameResult = someone.addName(fname: nil, lname: nil)
        XCTAssertFalse(addNameResult)
        
        let addNameResult2 = someone.addName(fname: "Apa", lname: "Banan")
        XCTAssertTrue(addNameResult2)
        XCTAssert(someone.firstname == "Apa")
        XCTAssert(someone.lastname == "Banan")
        
        let personeFullName = someone.getName()
        XCTAssert(personeFullName == "Apa Banan")
    }
    
    func testVCdouble()
    {
        let doubleResult = vc?.doubleSomething(inText: "7")
        
        XCTAssert(doubleResult == "X", "HEPP "+doubleResult!)
        
        XCTAssert(doubleResult == "14")
    }
    
    
    func testKollaTiden()
    {
        self.measure {
            var calc = 0
            
            for index in 0...10000
            {
                for _ in 0...10000
                {
                    calc = calc + index * 2
                    //calc = calc - calc*2
                }
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
